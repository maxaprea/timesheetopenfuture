import React, { Component } from 'react';
import Timesheet from './components/Timesheet';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Timesheet />
      </div>
    );
  }
}

export default App;
