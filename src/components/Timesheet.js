import React, {Component} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './Timesheet.css';

import moment from 'moment';

import axios from 'axios';

class Timesheet extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dipendente: {
                "id": 4,
                "codiceFiscale": "PPPPLT80A01H501V",
                "nome": "PIPPO",
                "cognome": "PLUTO",
                "dataDiNascita": "20190101",
                "email": "pippo@test.it",
                "commesseDipendente": [],
                "roleType": "ADMIN"
            },
            giornate: {
                "dipendente": {
                    "id": 4,
                    "nome": "PIPPO",
                    "cognome": "PLUTO",
                    "email": "pippo@test.it"
                },
                "id": 20,
                "annoMese": "201601",
                "approvato": false,
                "consolidato": false,
                "giornate": []
            },
            showNote: false,
            selectedNote: '',
            selectedDay: 0
        };

        this.updateTotals = this.updateTotals.bind(this);
        this.updateNote = this.updateNote.bind(this);
        this.updateSelectedDay = this.updateSelectedDay.bind(this);
        this.updateSelectedNote = this.updateSelectedNote.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.changeTimesheet = this.changeTimesheet.bind(this);
    }

    changeTimesheet = () => {
        axios.get(`../mock/timesheet${moment(this.state.startDate).format('MMYYYY')}.json`)
            .then( res =>
                this.setState({
                    dipendente: res.data.dipendente,
                    giornate: res.data.giornate,
                    mountTimesheet: false
                })
            )
            .catch( err => {console.log(err)});
    };

    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    updateSelectedNote = event => {
        const note = event.target.value;
        this.setState(state => ({
            selectedNote: note
        }))
    };

    updateSelectedDay = day => event => {
        if (this.state.selectedDay === day.day) {
            const show = !this.state.showNote;
            this.setState(state => ({showNote: show}))
        } else {
            this.setState(state => ({selectedDay: day.day, showNote: true, selectedNote: day.nota}))
        }
    };

    updateNote = () =>  {
        const timesheet = [...this.state.timesheet];
        const changedDayIndex = this.state.timesheet.findIndex( d => d.day === this.state.selectedDay);
        const changedDayNote = {...this.state.timesheet[changedDayIndex]};
        changedDayNote.nota = this.state.selectedNote;
        timesheet[changedDayIndex] = changedDayNote;

        this.setState(state =>({
            timesheet: timesheet,
            showNote: false
        }));
    };

    updateTotals = (day, commessa) => (event) => {

        const timesheet = [...this.state.timesheet];
        const changedDayIndex = this.state.timesheet.findIndex( d => d.dayComplete === day.dayComplete);
        const _i = timesheet[changedDayIndex].commessa.findIndex( c => c.nomeCommessa === commessa);
        timesheet[changedDayIndex].commessa[_i].oreCommessa = parseFloat(event.target.value);

        const commesseMese = [...this.state.commesse];
        const _iCommessa = commesseMese.findIndex(c => c.nomeCommessa === commessa);
        commesseMese[_iCommessa].oreCommessaMese = timesheet.map(giorno => giorno.commessa.find(c => c.nomeCommessa === commessa).oreCommessa).reduce((tot, ore) => tot + ore, 0);

        this.setState(state => ({
            timesheet: timesheet,
            commesse: commesseMese
        }));

    };

    isHoliday (day) {
        return moment(day.dayComplete, 'D/M/YYYY').weekday() === 6
            || moment(day.dayComplete, 'D/M/YYYY').weekday() === 0;
    }

    isToday (day) {
        return day.dayComplete === moment().format('DD/MM/YYYY');
    }

    generateTimesheet = mount => {
        const daysOfCurrentMonth = moment().daysInMonth();
        const currentMonth = moment().month()+1;
        const currentYear = moment().year();
        const commesse = this.state.dipendente.commesseDipendente.map( c => c.commessa.nome).map(c => {return {nomeCommessa: c, oreCommessaMese: 0}});

        const daysArray = [];
        for (let i=1; i<=daysOfCurrentMonth; i++)
        {
            const day = {
                dayComplete: moment(`${i}/${currentMonth}/${currentYear}`, 'D/M/YYYY').format('DD/MM/YYYY'),
                day: i,
                month: currentMonth,
                year: currentYear,
                nota: this.state.giornate.giornate.find( g => g.giorno === i) ?
                    this.state.giornate.giornate.find( g => g.giorno === i).nota ? this.state.giornate.giornate.find( g => g.giorno === i).nota : '' : '',
                commessa: commesse.map( c => {
                    return this.state.giornate.giornate.find( g => g.giorno === i) ?
                        {
                            nomeCommessa: c.nomeCommessa,
                            oreCommessa: this.state.giornate.giornate.find( g => g.giorno === i).giornataCommesse.find( cg => cg.commessa.nome === c.nomeCommessa).oreAssegnate
                        } :
                        {
                            nomeCommessa: c.nomeCommessa,
                            oreCommessa: 0
                        }

                })
            };
            daysArray.push(day);
        }

        commesse.map(c => {
            c.oreCommessaMese =
                daysArray.map(d => {
                    return d.commessa.find(co => co.nomeCommessa === c.nomeCommessa).oreCommessa
                }).reduce((tot, ore) => tot + ore, 0)
        });


        this.setState({
            timesheet: daysArray,
            commesse: commesse,
            mountTimesheet: mount
        });
    };

    componentDidMount() {
        console.log('componentDidMount');
        axios.get(`../mock/timesheet${moment(new Date()).format('MMYYYY')}.json`)
            .then( res =>
                this.setState({
                    dipendente: res.data.dipendente,
                    giornate: res.data.giornate,
                    mountTimesheet: false,
                    startDate: new Date()
                })
            )
            .catch( err => {console.log(err)});
        // this.generateTimesheet(this.state.startDate);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('componentDidUpdate', prevState);
        if (!this.state.mountTimesheet) {
            console.log('componentDidUpdate OK');
            this.generateTimesheet(true);
        }
    }

    render() {
console.log('render')

        if (this.state.timesheet && this.state.commesse) {
            const thead = <tr><th scope="col"></th>
                {
                    this.state.timesheet.map( (d, i) =>
                        <th key={i}
                            scope="col"
                            className={this.isHoliday(d) ? 'holiday': ''}>
                            {d.day}
                        </th>)
                }
                <th scope="col" className="bg-success text-white">Tot.</th>
            </tr>;

            const tbody = this.state.commesse.map( (c, i) => {
                return <tr key={i}>
                    <th scope="row" className="text-sm-left text-capitalize">{c.nomeCommessa}</th>
                    {
                        this.state.timesheet.map( (d, i) => {
                            return <td key={i} className={`${this.isHoliday(d) ? 'holiday': ''} ${this.isToday(d) && !this.isHoliday(d) ? 'bg-primary' : ''}`}>
                                <input className="form-control form-control-sm"
                                       type="number"
                                       step="0.5"
                                       min="0"
                                       onChange={this.updateTotals(d, c.nomeCommessa)}
                                       value={d.commessa.find( commessa => commessa.nomeCommessa === c.nomeCommessa).oreCommessa}/>
                            </td>
                        })
                    }
                    <td className="bg-success text-white no-side-padding">{c.oreCommessaMese}</td>
                </tr>
            });

            const isExtraTime = day => {
                return day.commessa.map(c => c.oreCommessa).reduce( (tot, n) => n + tot, 0);
            };

            const tfoot = <tr><th scope="col" className="text-left">Totali</th>
                {

                    this.state.timesheet.map( (d, i) =>
                        <th key={i}
                            scope="col"
                            className={`${this.isHoliday(d) ? 'holiday': ''} ${isExtraTime(d) > 8 ? 'bg-warning': ''}`}>
                            {isExtraTime(d)}
                        </th>)
                }
                <th className="bg-primary text-white no-side-padding">
                    {
                        this.state.commesse.reduce((tot, c) => tot + c.oreCommessaMese, 0)
                    }
                </th>
            </tr>;

            const note = <tr><th scope="col" className="text-left bg-dark note">Note</th>
                {

                    this.state.timesheet.map( (d, i) =>
                        <td key={i} className={`note${d.nota !== '' ? ' bg-info ': ' bg-dark'}`} onClick={this.updateSelectedDay(d)}>
                            <i className="fas fa-pen"></i>
                        </td>
                    )
                }
            </tr>;

            return (
                <div className="timesheet-container">
                    <div id="toolbar">
                        <DatePicker
                            selected={this.state.startDate}
                            onChange={this.handleChange}
                            dateFormat="MM/yyyy"
                            showMonthYearPicker
                        />
                        <button className="btn btn-primary btn-sm" onClick={this.changeTimesheet}>AGGIORNA</button>
                    </div>
                    <table className="table table-sm table-striped table-bordered">
                        <thead>
                        {thead}
                        </thead>
                        <tbody>
                        {tbody}
                        </tbody>
                        <tfoot className="bg-success">
                        {tfoot}
                        {note}
                        </tfoot>
                    </table>
                    {
                        this.state.showNote ?
                            <div className="note-container">
                                <textarea name="note" cols="30" rows="10" onChange={this.updateSelectedNote} value={this.state.selectedNote}></textarea>
                                <button className="btn btn-primary text-uppercase" onClick={this.updateNote}>salva</button>
                            </div>
                            : ''
                    }

                </div>
            );
        } else {
            return '';
        }

    }
}

export default Timesheet;
